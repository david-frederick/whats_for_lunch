class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:edit, :update, :destroy]

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all
    @recommendation = Restaurant.recommendation
    set_user
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
    set_user
  end

  # GET /restaurants/1/edit
  def edit
    set_user
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)

    respond_to do |format|
      if @restaurant.save
        flash[:success] = 'Restaurant was successfully created.'
        format.html { redirect_to restaurants_path }
        format.json { render :index, status: :created }
      else
        flash[:danger] = 'There was a problem creating the restaurant.'
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        flash[:success] = 'Restaurant was successfully updated.'
        format.html { redirect_to restaurants_path }
        format.json { render :index, status: :ok }
      else
        flash[:danger] = 'There was a problem updating the restaurant.'
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant.destroy
    respond_to do |format|
      flash[:success] = 'Restaurant was successfully destroyed.'
      format.html { redirect_to restaurants_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_restaurant
    @restaurant = Restaurant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def restaurant_params
    params.require(:restaurant).permit(:name, :last_visited)
  end

  def set_user
    if session[:current_user]
      @user = User.find_by(id: session[:current_user])
    else
      @user = User.new
    end
  end
end
