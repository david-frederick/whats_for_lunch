class UsersController < ApplicationController
  def create
    @user = User.find_or_initialize_by(user_params)

    respond_to do |format|
      if @user.save
        session[:current_user] = @user.id
        flash[:success] = 'User was successfully created.'
        format.html { redirect_to restaurants_path }
        format.json { render :index, status: :created }
      else
        flash[:danger] = 'There was a problem creating the user.'
        format.html { redirect_to restaurants_path }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:username)
  end
end
