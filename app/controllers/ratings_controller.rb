class RatingsController < ApplicationController
  def create
    @rating = Rating.find_or_initialize_by(rating_params)

    respond_to do |format|
      if @rating.save
        flash[:success] = 'Rating was successfully created.'
        format.html { redirect_to restaurants_path }
        format.json { render :index, status: :created }
      else
        flash[:danger] = 'There was a problem creating your rating.'
        format.html { redirect_to restaurants_path }
        format.json { render json: @rating.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @rating = Rating.find_or_initialize_by(rating_params)
    @rating.stars = params[:rating][:stars]

    respond_to do |format|
      if @rating.update(rating_params)
        flash[:success] = 'Rating was successfully updated.'
        format.html { redirect_to restaurants_path }
        format.json { render :index, status: :ok }
        format.js
      else
        flash[:danger] = 'There was a problem updating your rating.'
        format.html { redirect_to restaurants_path }
        format.json { render json: @rating.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private

  def rating_params
    params.require(:rating).permit(:user_id, :restaurant_id)
  end
end
