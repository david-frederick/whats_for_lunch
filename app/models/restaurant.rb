class Restaurant < ApplicationRecord
  include Custom::Avg
  has_many :ratings

  scope :visit_order, -> { order(last_visited: :asc) }

  def avg_rating
    Custom::Avg.avg(ratings.map { |r| r.stars }.reject { |r| r == nil})
  end

  def days_since_visit
    return 5 unless last_visited
    (Date.today - last_visited).to_i
  end

  def self.recommendation
    restaurants = Restaurant.visit_order.where("last_visited <= ? OR last_visited IS NULL", 1.day.ago)
    restaurants = Restaurant.visit_order if restaurants.empty?
    ratings = final_ratings(restaurants)
    ratings.max { |a,b| a[1] <=> b[1] }[0]
  end

  private

  def self.final_ratings(restaurants)
    avg_ratings(restaurants).map { |r| [r[0], r[1] + r[0].days_since_visit] }
  end

  def self.avg_ratings(restaurants)
    avgs = restaurants.map { |r| [r, r.avg_rating] }
    avgs.map { |r| r[1] == nil ? [r[0], 3] : r } # convert nils to 3
  end
end
