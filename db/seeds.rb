# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Restaurant.find_or_create_by!(name: 'Moe\'s Southwest Grill', last_visited: Date.strptime('04/17/2017', '%m/%d/%Y'))
Restaurant.find_or_create_by!(name: 'Blimpies')
cfa = Restaurant.find_or_create_by!(name: 'Chick-fil-A')
user = User.find_or_create_by!(username: 'test')
Rating.find_or_create_by!(stars: 4, user_id: user.id, restaurant_id: cfa.id)
