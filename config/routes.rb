Rails.application.routes.draw do
  resources :restaurants
  resources :users, only: :create
  resources :ratings, only: [:create, :update]
  root 'restaurants#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
