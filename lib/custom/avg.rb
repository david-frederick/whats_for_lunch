module Custom
  module Avg
    # takes an array of ints and returns the avg
    # returns nil for empty arrays
    # raises exception for non-numerical entries
    def self.avg(list)
      return nil if list.size == 0
      list.inject { |sum, n| sum + n } / list.size.to_f
    end
  end
end
